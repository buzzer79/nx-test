import { render } from '@testing-library/react';

import { Button } from './Button';

describe('DesignSystem', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<Button />);
    expect(baseElement).toBeTruthy();
  });
});
